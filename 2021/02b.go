package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {

	var pos, depth, aim int64

	// Open file
	file, err := os.Open("./input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// Read file
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if err != nil {
			log.Fatal(err)
		}
		line := strings.Split(scanner.Text(), " ")
		if len(line) < 2 {
			log.Fatal("unexpected input")
		}
		num, err := strconv.ParseInt(line[1], 10, 64)
		if err != nil {
			log.Fatal(err)
		}
		if line[0] == "forward" {
			pos += num
			depth += aim * num
		} else if line[0] == "down" {
			aim += num
		} else if line[0] == "up" {
			aim -= num
		} else {
			log.Fatal("unexpected input")
		}
	}
	fmt.Println(pos * depth)
}
