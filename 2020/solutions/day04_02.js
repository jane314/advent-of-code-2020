
// intersection of 2 arrays
function intersect(a, b) {
	let setB = new Set(b);
	return [...new Set(a)].filter(x => setB.has(x));
}

function valid(entry) {
	let byr = Number(entry.byr);
	if( byr < 1920 || byr > 2002 ) { return false; }
	let iyr = Number(entry.iyr);
	if( iyr < 2010 || iyr > 2020 ) { return false; }	
	let eyr = Number(entry.eyr);
	if( eyr < 2020 || eyr > 2030 ) { return false; }
	if( ! ( /^[0-9]*(in|cm)/.test(entry.hgt) ) ) { return false; }
	let hgtnum = Number(entry.hgt.match(/[0-9]*/g).filter( t => t != "").join(""));
	let hgtunit = entry.hgt.match(/(in|cm)/g).filter( t => t != "" ).join("");
	if( hgtunit == "in" && ( hgtnum < 59 || hgtnum > 76 ) ) { return false; }
	if( hgtunit == "cm" && ( hgtnum < 150 || hgtnum > 193 ) ) { return false; }
	if( ! ( /^#[0-9a-f]{6}$/.test(entry.hcl) ) ) { return false; }
	if( [ "amb", "blu", "brn", "gry", "grn", "hzl", "oth" ].indexOf(entry.ecl) == -1 ) { return false; }
	return /^[0-9]{9}$/.test(entry.pid);
}

const fields = [ "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" ];

const data = JSON.parse(Deno.readTextFileSync('output.txt'));

let count = 0;
data.forEach( entry => {
	if( intersect( Object.keys(entry), fields ).length == fields.length ) {
		if ( valid(entry) ) {	
			count += 1;
		}
	}
} );

console.log( count );
