let inputaux = Deno.readTextFileSync('output.txt').split('\n').filter( str => str.length != 0 )
	.map( str => str.split(' ') ).map( arr => arr.filter( str => str.length != 0) );

let input = {}; 
inputaux.forEach( arr => {
		let key = `${arr[0]}_${arr[1]}`; 
		let tmparr = [ ];
		for( let i = 2; i < arr.length; i += 3 ) {
			tmparr.push( { num: Number(arr[i]), color: `${arr[i+1]}_${arr[i+2]}` } );	
		}
		input[key] = tmparr;
} );
	
function gen(iter) {
	return iter.map( obj => { return mult( obj.num, input[obj.color] );  } ).flat();
}

function mult(n, iter) {
	return iter.map( obj => { return { num: obj.num * n, color: obj.color }; } );
}

function count(iter) {
	let y = iter.map( obj => obj.num );
	if( y.length == 0 ) { return 0; }
	return y.reduce( (a,b) => a+b );
}

let x = [ { num: 1 , color:'shiny_gold' } ];
let tmp = -1;
let total = 0;

while( tmp != 0 ) {
	x = gen(x);
	tmp = count(x);
	total += tmp;
}
console.log(total);

