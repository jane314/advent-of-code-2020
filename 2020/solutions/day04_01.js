
// intersection of 2 arrays
function intersect(a, b) {
	let setB = new Set(b);
	return [...new Set(a)].filter(x => setB.has(x));
}

const fields = [ "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" ];

const data = JSON.parse(Deno.readTextFileSync('output.txt'));

let count = 0;
data.forEach( entry => {
	if( intersect( Object.keys(entry), fields ).length == fields.length ) {
		count += 1;
	}
} );

console.log( count );