
const input: string = Deno.readTextFileSync('output.txt');
const array: string[][] = input.split('\n').map( str => str.split(',') );
array.pop();

let count = 0;

function xor(a: boolean, b: boolean): boolean {
	return (a || b) && ( !(a && b) );
}

array.forEach( ( arr: string[] ) => {
	let n1 = Number(arr[0]);
	let n2 = Number(arr[1]);
	let str = arr[3].padEnd(n2 + 1);
	if( xor( str[n1 - 1] == arr[2], str[n2 - 1] == arr[2] ) ) {
		count += 1;
	}
} );

console.log(count);

