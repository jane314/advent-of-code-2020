#!/bin/bash
rm output.txt
echo "[{" >>output.txt
cat input.txt | sed -e 's/\([A-Za-z0-9]*\):\([A-Za-z0-9#]*\)/\"\1\":\"\2\",/g' | sed -e 's/^[[:space:]]*$/},{/g' | tr -d '\n' | sed -e 's/,[[:space:]]*}/}/g' | sed -e 's/,$//g' >>output.txt
echo "}]" >>output.txt
deno run --allow-read day04_02.js