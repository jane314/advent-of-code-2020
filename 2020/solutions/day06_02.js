// intersection of 2 arrays
function intersect(a, b) {
	let setB = new Set(b);
	return [...new Set(a)].filter( setB.has );
}

const input = Deno.readTextFileSync('output.txt')
	.split(',')
	.map( str => str.split('\n'))
	.map( arr => arr.filter( str => str.length != 0 ).map((str) => str.split('')) )
	.map( arr => arr.reduce(intersect))
	.map( arr => arr.length).reduce( (a, b) => a + b );
console.log(input);
