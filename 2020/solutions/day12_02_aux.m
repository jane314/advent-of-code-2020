% N S E W L R F

deg2rad = @(t) ( pi / 180) * t;
rot= @(t) [ cos(deg2rad(t)) -sin(deg2rad(t)); sin(deg2rad(t)) cos(deg2rad(t)) ];

ship = [0; 0];
waypoint = [10; 1];

for i = 1:1:size(input)(1)
	if input(i, 1) == 0
		waypoint += input(i, 2) .* [0; 1];
	elseif input(i, 1) == 1
		waypoint += input(i, 2) .* [0; -1];
	elseif input(i, 1) == 2
		waypoint += input(i, 2) .* [1; 0];
	elseif input(i, 1) == 3
		waypoint += input(i, 2) .* [-1; 0];
	elseif input(i, 1) == 4
		waypoint = rot( input(i, 2) ) * waypoint;
	elseif input(i, 1) == 5
		waypoint = rot( -input(i, 2) ) * waypoint;
	elseif input(i, 1) == 6
		ship += input(i, 2) .* waypoint;
	else
	
	endif
endfor

disp(round(ship));
disp(dot(round(abs(ship)), [1, 1] ));
