
deg2rad = @(t) ( pi / 180) * t;
rot= @(t) [ cos(deg2rad(t)) -sin(deg2rad(t)); sin(deg2rad(t)) cos(deg2rad(t)) ];
pos = [0, 0];
dir = [1; 0];

for i = 1:1:size(input)(1)
	if input(i, 1) == 0
		pos += input(i, 2) .* [0, 1];
	elseif input(i, 1) == 1
		pos += input(i, 2) .* [0, -1];
	elseif input(i, 1) == 2
		pos += input(i, 2) .* [1, 0];
	elseif input(i, 1) == 3
		pos += input(i, 2) .* [-1, 0];
	elseif input(i, 1) == 4
		dir = rot( input(i, 2) ) * dir;
	elseif input(i, 1) == 5
		dir = rot( -input(i, 2) ) * dir;
	elseif input(i, 1) == 6
		pos += input(i, 2) .* transpose( dir );
	else
		disp('here');
	endif
endfor

disp(round(pos));
disp(dot(round(abs(pos)), [1, 1] ));
